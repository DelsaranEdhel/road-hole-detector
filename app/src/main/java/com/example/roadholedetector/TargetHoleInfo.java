package com.example.roadholedetector;

import android.location.Location;

public class TargetHoleInfo
{
    public Location startLocation; //current user location
    public Location holeLocation; //location of a hole
    public int holeStrength;
    public double meters; //meters from current user location to location of a hole
    public int ID; //ID of this hole in the database

    public TargetHoleInfo()
    {
        startLocation = null;
        holeLocation = null;
        holeStrength = -1;
        ID = -1;
        meters = Integer.MAX_VALUE;
    }

    public boolean IsNull()
    {
        return startLocation == null || holeLocation == null;
    }
}
