package com.example.roadholedetector;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

//Class which draws a hole

public class DrawHole implements View.OnTouchListener
{
    public static ArrayList<PointF> points = new ArrayList<>(); //store current points for current hole
    public static boolean isDrawing = false; //are points being currently draw

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        if(isDrawing)
        {
            PointF point = new PointF(motionEvent.getX(), motionEvent.getY());
            points.add(point);

            DrawShape();
        }

        return false;
    }

    //Converts points array to one string (used to store in database)
    public static String ConvertPointsToString()
    {
        String finalText = "";

        String pointX, pointY;

        for(PointF point : points)
        {
            pointX = String.valueOf(point.x);
            pointY = String.valueOf(point.y);

            //"," - separates x and y cords
            //"\n" - separates each pair of points
            finalText += pointX + "," + pointY + "\n";
        }

        /*
        Format of final text
        "
        1.234,5.321
        9.523,7.534
        5.234,8.342
        "
         */
        return finalText;
    }

    //Converts one string which contains all points to points array (used to read from database)
    public static void ConvertStringToPoints(String text)
    {
        ArrayList<PointF> newPoints = new ArrayList<>();

        String[] points = text.split("\n");

        for(String point : points)
        {
            String[] cords = point.split(",");

            PointF newPoint = new PointF(Float.parseFloat(cords[0]), Float.parseFloat(cords[1]));
            newPoints.add(newPoint);
        }

        DrawHole.points = newPoints;

        DrawShape();
    }

    //Draw a shape -> draw points and lines between them
    private static void DrawShape()
    {
        Bitmap bitmap = Bitmap.createBitmap(MainActivity.drawHoleImage.getWidth(), MainActivity.drawHoleImage.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);

        //draw points
        for(PointF point : points)
            canvas.drawCircle(point.x, point.y, 20, paint);

        //draw lines
        if(points.size() > 1)
        {
            for(int i = 0; i < points.size(); i++)
            {
                if(i+1 > points.size() - 1)
                    break;

                //draw lines between neighbour points
                canvas.drawLine(points.get(i).x, points.get(i).y, points.get(i+1).x, points.get(i+1).y, paint);
            }

            //draw last line, between first and last point
            canvas.drawLine(points.get(0).x, points.get(0).y, points.get(points.size()-1).x, points.get(points.size()-1).y, paint);
        }

        MainActivity.drawHoleImage.setImageBitmap(bitmap);
    }
}
