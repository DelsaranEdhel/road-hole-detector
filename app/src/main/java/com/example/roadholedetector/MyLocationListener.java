package com.example.roadholedetector;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class MyLocationListener implements LocationListener
{
    public static String lat, lon;

    public static Location myLocation;

    @Override
    public void onLocationChanged(Location loc)
    {
        lat = String.valueOf(loc.getLatitude());
        lon = String.valueOf(loc.getLongitude());

        myLocation = loc;
        Log.d("GPSGPS", "Location changed: lat = " + lat + ", lon = " + lon);
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
}
