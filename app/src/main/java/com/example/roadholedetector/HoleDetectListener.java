package com.example.roadholedetector;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class HoleDetectListener implements SensorEventListener
{
    private MainActivity mainActivity;

    private double acceleration = 0f;
    private double currentAcceleration = 0f;
    private double lastAcceleration = 0f;

    private String holeStrengthEditText;
    private double holeDetectThreshold;

    public HoleDetectListener(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        double x = sensorEvent.values[0];
        double y = sensorEvent.values[1];
        double z = sensorEvent.values[2];
        lastAcceleration = currentAcceleration;

        currentAcceleration = Math.sqrt(x * x + y * y + z * z);
        double delta = currentAcceleration - lastAcceleration;
        acceleration = acceleration * 0.9f + delta;

        holeStrengthEditText = mainActivity.holeStrengthEditText.getText().toString();
        if(holeStrengthEditText.equals(""))
            holeDetectThreshold = 5;
        else
        {
            holeDetectThreshold = Double.parseDouble(holeStrengthEditText);
            if(holeDetectThreshold < 0)
                holeDetectThreshold = 5;
        }

        if (acceleration > holeDetectThreshold)
            mainActivity.HoleDetected(acceleration);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i)
    {

    }
}
