package com.example.roadholedetector;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ipsec.ike.exceptions.InvalidSelectorsException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String dbName = "baza_dziur.db";
    private SQLiteDatabase database = null;

    private LocationManager locationManager;
    private SensorManager sensorManager;

    private MyLocationListener myLocationListener;
    private CompassListener compassListener;
    private HoleDetectListener holeDetectListener;

    private Location lastHoleLocation;
    private double lastHoleStrength;
    private boolean wasHoleDetected = false;

    private TextView holeDetectText;
    private Button saveHoleButton;
    private Button cancelHoleButton;

    public TextView distanceToHoleText;
    public ImageView compassImage;
    public EditText holeStrengthEditText;

    private Handler cancelHoleHandler; //handler that executes CancelHole() after 5 seconds

    private Button saveDrawHoleButton;
    private Button cancelDrawHoleButton;
    public  static ImageView drawHoleImage;
    private DrawHole drawHole;

    public static int holesCount = 0; //keep the counter to save the ID of the hole in the database
    public static int pointsCount = 0; //keep the counter to save the ID of the points in the database

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        myLocationListener = new MyLocationListener();

        distanceToHoleText = findViewById(R.id.distanceToHoleText);
        compassImage = findViewById(R.id.compassImage);
        holeStrengthEditText = findViewById(R.id.holeStrengthEditText);




        saveDrawHoleButton = findViewById(R.id.saveHoleDrawButton);
        cancelDrawHoleButton = findViewById(R.id.cancelHoleDrawButton);
        drawHoleImage = findViewById(R.id.drawHoleImage);

        saveDrawHoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveDrawHole();
            }
        });

        cancelDrawHoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CancelDrawHole();
            }
        });

        drawHole = new DrawHole();
        drawHoleImage.setOnTouchListener(drawHole);



        compassImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SwitchImages();
            }
        });

        drawHoleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SwitchImages();
            }
        });




        holeDetectText = findViewById(R.id.holeDetectText);
        saveHoleButton = findViewById(R.id.saveHoleButton);
        cancelHoleButton = findViewById(R.id.cancelHoleButton);

        saveHoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveHole();
            }
        });
        cancelHoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CancelHole();
            }
        });


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Not Enough Permission", Toast.LENGTH_SHORT).show();
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, myLocationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, myLocationListener);

        compassListener = new CompassListener(this);
        sensorManager.registerListener(compassListener, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), 2);

        holeDetectListener = new HoleDetectListener(this);
        sensorManager.registerListener(holeDetectListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 1000000);

        try {
            database = this.openOrCreateDatabase(dbName, MODE_PRIVATE, null);
            database.execSQL("DROP TABLE Holes");
            database.execSQL("CREATE TABLE IF NOT EXISTS Holes (Longitude TEXT, Latitude TEXT, HoleStrength INTEGER, ID INTEGER)");

            database.execSQL("DROP TABLE Points");
            database.execSQL("CREATE TABLE IF NOT EXISTS Points (Points TEXT, ID INTEGER)");
        } catch (SQLiteException e) {
            Log.e(getClass().getSimpleName(), "Could not create or open the database");
        }

        SetDrawHoleVisibility(View.INVISIBLE);
        SetHoleDetectedMessageVisibility(View.INVISIBLE);

        //prevent from this error: "W/System: A resource failed to call release"
        StrictMode.enableDefaults();
    }

    public Location GetCurrentLocation()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null)
                continue;
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy())
                bestLocation = l;
        }

        return bestLocation;
    }

    //when hole was detected, save the latest hole data
    public void HoleDetected(double holeStrength)
    {
        if (wasHoleDetected)
            return;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        lastHoleLocation = GetCurrentLocation();
        lastHoleStrength = holeStrength;
        wasHoleDetected = true;

        SetHoleDetectedMessageVisibility(View.VISIBLE);

        cancelHoleHandler = new Handler(Looper.getMainLooper());
        cancelHoleHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                CancelHole();
            }
        }, 5000);
    }

    private void SaveHole()
    {
        String lat = String.valueOf(lastHoleLocation.getLatitude());
        String lon = String.valueOf(lastHoleLocation.getLongitude());
        String holeStrength = String.valueOf((int)lastHoleStrength);
        AddHoleToDatabase(lon, lat, holeStrength);

        Log.d("GPS", "Hole saved at: " + lat + " | " + lon + "with strength = " + holeStrength);

        CancelHole();

        SetDrawHoleVisibility(View.VISIBLE);
    }

    private void CancelHole()
    {
        lastHoleLocation = null;
        lastHoleStrength = -1;
        wasHoleDetected = false;
        SetHoleDetectedMessageVisibility(View.INVISIBLE);
        cancelHoleHandler.removeCallbacks(null);
    }

    //sets the visibility of the info about detected hole and options to save it or cancel it
    private void SetHoleDetectedMessageVisibility(int visibility)
    {
        holeDetectText.setVisibility(visibility);
        saveHoleButton.setVisibility(visibility);
        cancelHoleButton.setVisibility(visibility);
    }

    private void SaveDrawHole()
    {
        //convert points to string to save it in the database
        String pointsInText = DrawHole.ConvertPointsToString();
        database.execSQL("INSERT INTO Points Values('" + pointsInText + "', '" + String.valueOf(pointsCount) + "')");

        pointsCount++;
        SetDrawHoleVisibility(View.INVISIBLE);
    }

    private void CancelDrawHole()
    {
        pointsCount++;
        SetDrawHoleVisibility(View.INVISIBLE);
    }

    //sets the visibility of the menu to draw a hole, save it or cancel it
    private void SetDrawHoleVisibility(int visibility)
    {
        if(visibility == View.VISIBLE)
            DrawHole.isDrawing = true;
        else
            DrawHole.isDrawing = false;

        saveDrawHoleButton.setVisibility(visibility);
        cancelDrawHoleButton.setVisibility(visibility);
        drawHoleImage.setVisibility(visibility);

        switch(visibility)
        {
            case View.VISIBLE:
                compassImage.setVisibility(View.INVISIBLE);
                break;
            case View.INVISIBLE:
                compassImage.setVisibility(View.VISIBLE);
                break;
        }
    }

    //When hole is detected we can click on the compass to show a drawing, and then click on a drawing to show the compass
    private void SwitchImages()
    {
        if(CompassListener.isDetectingHole && !DrawHole.isDrawing)
        {
            if(compassImage.getVisibility() == View.VISIBLE)
            {
                compassImage.setVisibility(View.INVISIBLE);
                drawHoleImage.setVisibility(View.VISIBLE);
            }
            else
            {
                compassImage.setVisibility(View.VISIBLE);
                drawHoleImage.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void AddHoleToDatabase(String lon, String lat, String holeStrength)
    {
        database.execSQL("INSERT INTO Holes Values('" + lon + "', '" + lat + "', '" + holeStrength + "', '" + String.valueOf(holesCount) + "')");
        holesCount++;
    }

    //Returns the hole which is the closest to the current user location
    public TargetHoleInfo GetClosestHole()
    {
        Cursor cursor = database.rawQuery("SELECT * FROM Holes", null);

        Location currentLocation = GetCurrentLocation();
        if(currentLocation == null)
            return null;

        double currentLat = currentLocation.getLatitude();
        double currentLon = currentLocation.getLongitude();

        int targetHoleStrength, targetHoleID;
        double targetHoleLat, targetHoleLon, tempDistanceInMeters;
        TargetHoleInfo targetHoleInfo = new TargetHoleInfo();

        //cursor.getString(0) - lon
        //cursor.getString(1) - lat
        //cursor.getString(2) - holeStrength
        //cursor.getString(3) - ID

        if(cursor.moveToFirst())
        {
            do
            {
                targetHoleID = Integer.parseInt(cursor.getString(3));
                targetHoleStrength = Integer.parseInt(cursor.getString(2));
                targetHoleLat = Double.parseDouble(cursor.getString(1));
                targetHoleLon = Double.parseDouble(cursor.getString(0));

                tempDistanceInMeters = DistanceBetweenPointsInMeters((float)currentLat, (float)currentLon, (float)targetHoleLat, (float)targetHoleLon);

                if(tempDistanceInMeters < targetHoleInfo.meters)
                {
                    Location location = new Location("targetHole");
                    location.setLatitude(targetHoleLat);
                    location.setLongitude(targetHoleLon);

                    targetHoleInfo.meters = tempDistanceInMeters;
                    targetHoleInfo.startLocation = currentLocation;
                    targetHoleInfo.holeLocation = location;
                    targetHoleInfo.holeStrength = targetHoleStrength;
                    targetHoleInfo.ID = targetHoleID;
                }
            } while(cursor.moveToNext());
        }

        cursor.close();
        return targetHoleInfo;
    }

    //Based on given hole ID returns a points of a hole drawing
    public String GetPointsOfClosestHole(int holeID)
    {
        Cursor cursor = database.rawQuery("SELECT * FROM Points", null);

        int pointsID;

        //cursor.getString(0) - points converted in text
        //cursor.getString(1) - ID

        if(cursor.moveToFirst())
        {
            do
            {
                pointsID = Integer.parseInt(cursor.getString(1));

                if(holeID == pointsID)
                {
                    String result = cursor.getString(0);
                    cursor.close();
                    return result;
                }

            } while(cursor.moveToNext());
        }

        return null;
    }

    private double DistanceBetweenPointsInMeters(float lat_a, float lng_a, float lat_b, float lng_b)
    {
        float pk = (float) (180.f/Math.PI);

        float a1 = lat_a / pk;
        float a2 = lng_a / pk;
        float b1 = lat_b / pk;
        float b2 = lng_b / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        sensorManager.unregisterListener(compassListener);
        sensorManager.unregisterListener(holeDetectListener);
        locationManager.removeUpdates(myLocationListener);

        if(database != null)
        {
            database.execSQL("DELETE FROM Points");
            database.execSQL("DELETE FROM Holes");
            database.close();
        }
    }
}