package com.example.roadholedetector;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.net.wifi.WifiManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import java.text.DecimalFormat;

public class CompassListener implements SensorEventListener
{
    private MainActivity mainActivity;
    private DecimalFormat df = new DecimalFormat("#.##");

    private float degreeStart = 0f;
    public static boolean isDetectingHole = false;

    public CompassListener(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        TargetHoleInfo targetHoleInfo = mainActivity.GetClosestHole();

        //when the distance is more than 50 meters then stop showing holes
        if(targetHoleInfo == null || targetHoleInfo.IsNull()|| targetHoleInfo.meters > 50)
        {
            isDetectingHole = false;
            mainActivity.distanceToHoleText.setText("Detecting holes...");
            mainActivity.compassImage.setRotation(0);
            return;
        }

        isDetectingHole = true;

        // get angle around the z-axis rotated
        mainActivity.distanceToHoleText.setText("Distance: " + df.format(targetHoleInfo.meters) + " m");

        //get points of a current hole to draw a shape
        String pointsText = mainActivity.GetPointsOfClosestHole(targetHoleInfo.ID);
        if(pointsText != null)
            DrawHole.ConvertStringToPoints(pointsText);

        float degree = targetHoleInfo.startLocation.bearingTo(targetHoleInfo.holeLocation);
        RotateAnimation ra = new RotateAnimation(
                degreeStart,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        degreeStart = -degree;
        ra.setFillAfter(true);
        //set how long the animation for the compass image will take place
        ra.setDuration(0);
        //start animation of a compass image
        //(only if compassImage is visible, because if it is invisible, then by starting an animation the view will become visible)
        if(mainActivity.compassImage.getVisibility() == View.VISIBLE)
            mainActivity.compassImage.startAnimation(ra);

        degreeStart = -degree;



        /* Nie dziala - strzalka "wariuje" zamiast wskazywac na lokalizacje
        mainActivity.distanceToHoleText.setText("Distance: " + df.format(targetHoleInfo.meters) + " m");

        float azimuth = sensorEvent.values[0]; // get azimuth from the orientation sensor
        Location currentLoc = MyLocationListener.myLocation; // targetHoleInfo.startLocation; // get location from GPS or network
        // convert radians to degrees
        azimuth = (float) Math.toDegrees(azimuth);
        GeomagneticField geoField = new GeomagneticField(
                (float) currentLoc.getLatitude(),
                (float) currentLoc.getLongitude(),
                (float) currentLoc.getAltitude(),
                System.currentTimeMillis());
        azimuth += geoField.getDeclination(); // converts magnetic north to true north
        float bearing = currentLoc.bearingTo(targetHoleInfo.holeLocation); // (it's already in degrees)
        float direction = azimuth - bearing;

        mainActivity.compassImage.setRotation(direction);

        Log.d("GPS", String.valueOf(targetHoleInfo.startLocation) + " | " + String.valueOf(targetHoleInfo.holeLocation));
         */
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i)
    {

    }
}