# Road Hole Detector

App written in Android Studio using Java and SQLite. App uses a lot of device's sensors (accelerometer, gyroscope, location).
Detects a holes on the roads, saves them and then shows to the user the direction and distance to the nearest hole.

When accelerometer detects a hole, the app asks if the user wants to save it. If the user selectes to save this hole, he then can draw a shape of this hole. After that the hole is being saved in the database (SQLite).
The app also contains an arrow which shows in which direction there is a nearest hole that was saved by the user and also shows a distance to that hole (in meters).
